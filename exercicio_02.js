/* Escreva um algoritmo para ler uma temperatura em graus Fahrenheit,
calcular e escrever o valor correspondente em graus Celsius (baseado na
fórmula abaixo): */

const converterCelsius = (f) => {
    return (5 * (f-32)/9)
}

let tempf = Math.round(Math.random()*(100));

console.log(tempf +"° fahrenheit = " + converterCelsius(tempf) + "° celsius.");