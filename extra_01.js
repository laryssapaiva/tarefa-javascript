/* 1. Faça um programa que leia 6 valores. Estes valores são somente
negativos ou positivos (desconsidere os valores nulos). A seguir, mostre a
quantidade de valores positivos digitados. */


let array = [-1, 5, 12, -6, 23, 7];
let positivos=0;

for (let i=0; i<6; i++){
    if (array[i] > 0){
        positivos++;
    }
}

console.log(positivos);