/* 5. Teste 5 números inteiros aleatórios. Os testes:
● Caso o valor seja divisível por 3, imprima no console “fizz”.
● Caso o valor seja divisível por 5 imprima “buzz”.
● Caso o valor seja divisível por 3 e 5, ao mesmo tempo, imprima
“fizzbuzz”.
● Caso contrário imprima nada. */

var numeros = [5, 10, 15, 22, 45];
let fb="";

for (n of numeros){
    if (n%3==0){
        fb = "fizz"
    }
    if (n%5==0){
        fb += "buzz";
    }
    console.log(fb);
    fb = "";
}