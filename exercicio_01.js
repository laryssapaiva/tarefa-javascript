/*  Implemente um algoritmo que pegue duas matrizes (array de arrays) e
realize sua multiplicação. Lembrando que para realizar a multiplicação
dessas matrizes o número de colunas da primeira matriz tem que ser
igual ao número de linhas da segunda matriz. (2x2) */

let matriz1 = [[5,5], [5,5]];
let matriz2 = [[2,2], [2,2]];
let matriz3 = [[0,0], [0,0]];

for(let i=0; i<2; i++){
    for(let j=0; j<2; j++){
        matriz3[i][j] = matriz1[i][j] * matriz2[i][j];
    }
}

console.log(matriz3);