/* 4. Crie um algoritmo que transforma as notas do sistema numérico para
sistema de notas em caracteres Tipo A, B e C
● hde 90 para cima - A
● entre 80 e 90 -B
● entre 70 e 79 - C
● entre 60 e 69 - D
● menor que 60 - F
 */

let nota = 100;

if (nota>90){
    console.log('A');
} else if (nota>=80 && nota<90){
    console.log('B');
} else if (nota>=70 && nota<80){
    console.log('C');
} else if (nota>=60 && nota<70){
    console.log('D');
} else if (nota<60){
    console.log('F');
}