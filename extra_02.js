/* 2. Implemente uma função recursiva que,dados dois números inteiros x e
n, calcula o valor de xn */

let x = 10;
let n = 2;

function multiplica(a, b){
    if (a == 0){
        return 0;
    } else {
        return b + multiplica(a, b-1);
    }
}

console.log(multiplica(10, 2));